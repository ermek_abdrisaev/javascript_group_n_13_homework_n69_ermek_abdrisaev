import { Component } from '@angular/core';

@Component({
  selector: 'app-thanx',
  template: `<h1>Thank you for registration. We will respond as soon as possible</h1>`,
  styles: [`h1{color: blue;}`],
})

export class ThanxComponent{}
