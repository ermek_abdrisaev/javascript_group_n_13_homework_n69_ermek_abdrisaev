import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { RegisterService } from '../shared/register.service';
import { Router } from '@angular/router';
import { Register } from '../shared/register.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {

  @ViewChild('f') userForm!: NgForm;

  isUploading = false;
  registerUploadingSubscription!: Subscription;

  constructor(
    private registerService: RegisterService,
    private router: Router,
  ){}

  ngOnInit(): void {
    this.registerUploadingSubscription = this.registerService.registerUploading.subscribe((isUploading: boolean) =>{
      this.isUploading = isUploading;
    })
  }

  onSubmit(){
    const id = Math.random().toString();

    const register = new Register(
      id,
      this.userForm.value.firstName,
      this.userForm.value.lastName,
      this.userForm.value.sureName,
      this.userForm.value.size,
      this.userForm.value.phoneNumber,
      this.userForm.value.placeOfStudy,
      this.userForm.value.gender,
      this.userForm.value.comment,

    );


    this.registerService.addRegister(register).subscribe();
    void this.router.navigate(['/thanx']);
  }

  ngOnDestroy(): void {
    this.registerUploadingSubscription.unsubscribe();
  }

}
