import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive } from '@angular/core';

export const nameValidator = () : ValidatorFn =>{
  return (control: AbstractControl): ValidationErrors | null =>{
    const hasLetter = /[a-zA-Z]/.test(control.value);
    if(hasLetter){
      return null;
    }
    return {name: true};
  }
};

@Directive({
  selector: '[appName]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateNameDirective,
    multi: true,
  }]
})

export class  ValidateNameDirective implements  Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return nameValidator()(control);
  }

}
