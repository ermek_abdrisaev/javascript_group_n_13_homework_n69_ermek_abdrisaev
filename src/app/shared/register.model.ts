export class Register {
  constructor(
    public id: string,
    public firstName: string,
    public lastName: string,
    public sureName: string,
    public size: string,
    public phoneNumber: number,
    public placeOfStudy: string,
    public gender: string,
    public comment: string,
  ){}
}
