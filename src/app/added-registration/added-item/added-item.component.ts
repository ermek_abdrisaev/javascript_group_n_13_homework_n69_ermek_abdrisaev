import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { RegisterService } from '../../shared/register.service';
import { Register } from '../../shared/register.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-added-item',
  templateUrl: './added-item.component.html',
  styleUrls: ['./added-item.component.css']
})
export class AddedItemComponent implements OnInit, OnDestroy {
  @Input() register!: Register;
  registerId = '';
  isRemoving = false;
  registerDeleteSubscription!: Subscription;

  constructor(
    private registerService: RegisterService) { }

  ngOnInit(): void {
    this.registerId = this.register.id;
    this.registerDeleteSubscription = this.registerService.registerRemoving.subscribe((isRemoving: boolean) =>{
      this.isRemoving = isRemoving;
    })
  }

  onDeleteRegister(id: string){
    this.registerService.removeRegister(id).subscribe(() =>{
      this.registerService.fetchRegisters();
    })
  }

  ngOnDestroy(): void {
    this.registerDeleteSubscription.unsubscribe();
  }

}
