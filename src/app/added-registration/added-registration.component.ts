import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Register } from '../shared/register.model';
import { Subscription } from 'rxjs';
import { RegisterService } from '../shared/register.service';

@Component({
  selector: 'app-added-registration',
  templateUrl: './added-registration.component.html',
  styleUrls: ['./added-registration.component.css']
})
export class AddedRegistrationComponent implements OnInit, OnDestroy {
  registers!: Register[];
  registersChangeSubscription!: Subscription;
  registersFetchingSubscription!: Subscription;
  loading: boolean = false;

  constructor(
    private registerService: RegisterService) {}

  ngOnInit(): void {
    this.registersChangeSubscription = this.registerService.registerChange.subscribe((registers: Register[]) =>{
      this.registers = registers;

    });
    this.registersFetchingSubscription = this.registerService.registerFetching.subscribe((isFetching: boolean) =>{
      this.loading = isFetching;
    });
    this.registerService.fetchRegisters();
  }

  ngOnDestroy(): void {
    this.registersChangeSubscription.unsubscribe();
    this.registersFetchingSubscription.unsubscribe();
  }

}
