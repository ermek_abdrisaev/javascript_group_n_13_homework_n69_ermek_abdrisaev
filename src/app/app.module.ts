import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ValidateNameDirective } from './validate-name.directive';
import { ValidateNumberDirective } from './validate-number.directive';
import { RegisterService } from './shared/register.service';
import { HttpClientModule } from '@angular/common/http';
import { ThanxComponent } from './thanx.component';
import { FormComponent } from './form/form.component';
import { AddedRegistrationComponent } from './added-registration/added-registration.component';
import { AddedItemComponent } from './added-registration/added-item/added-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ValidateNameDirective,
    ValidateNumberDirective,
    ThanxComponent,
    FormComponent,
    AddedRegistrationComponent,
    AddedItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
